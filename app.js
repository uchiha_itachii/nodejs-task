const express = require('express');
const bodyParser = require('body-parser');
const router = require('./router');
const path = require('path');
const PORT = process.env.PORT || 3000;

const app = express();

//Body Parser Middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended : false}));

//Set Static Path
app.use(express.static(path.join(__dirname,'public')))

//Set View Engine-EJS
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname,'views'));


app.use('/',router)



app.listen(PORT, () => console.log(`Server is running on port ${PORT}`))
