const express = require('express');
const fs = require('fs');
const router = express.Router();
const nodemailer = require('nodemailer');
const twilio = require('twilio');
const path = require('path');



router.get('/', (req,res) => {
    res.sendFile(path.join(__dirname,"public","./form.html"));
});

router.post('/submit', (req,res) => {
    const {name, email, phone, message} = req.body;
    const user = req.body

    //Store User Data in json file
    fs.readFile('./data/users.json', (err, data) => {
      if (err && err.code === "ENOENT") {
          return fs.writeFile('./data/users.json', JSON.stringify([user]), error => console.log(error));
      }
      else if (err) {
          console.error(err);
      }    
      else {
          try {
              const fileData = JSON.parse(data);
  
              fileData.push(user);
  
              return fs.writeFile('./data/users.json', JSON.stringify(fileData), error => console.log(error))
          } catch(exception) {
              console.error(exception);
          }
      }
  });
    //Email Notification
    var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
          user: 'xyz@gmail.com',
          pass: 'password'
        }
      });
      
      var mailOptions = {
        from: 'xyz@gmail.com',
        to: 'debottam.bhatt@webspiders.com',
        subject: 'User Information',
        html: `<ul>
        <li>Name: ${name}<li/>
        <li>Email: ${email}<li/>
        <li>Phone: ${phone}<li/>
        <li>Message: ${message}<li/>
        </ul>`
      };
      
      transporter.sendMail(mailOptions,  (error, info) => {
        if (error) {
          console.log(error);
        } else {
          console.log('Email sent: ' + info.response);
        }
      });

    //SMS Notification
        const accountSid = 'ACXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX';
        const authToken = 'your_auth_token';
        const client = twilio(accountSid, authToken);

        client.messages
        .create({
            body: `Name: ${name}, Email: ${email}, Phone: ${phone}, Message: ${message}`,
            from: '+917277068914',
            to: '+919830822334'
        })
        .then(message => console.log(message.sid));
        
    //Response to Client
    res.render('user', {name: name,
                        email: email,
                        phone: phone,
                        message: message
    })
})

module.exports = router;

