# README #

This README documents whatever steps are necessary to get the application up and running.

### What is this repository for? ###

*This repository contains all the files created with Node.js using other libraries.

### How do I get set up? ###

*In order to run the files, certain node modules and dependies are needed to be added.
*No node modules are included in this package so all the libraries need to be added manually.
*All the dependencies have been added in the package.json file.
*To download and install these dependencies, use 'npm i' or 'npm install' command from the command line.
*After all the dependencies have been added, use 'node app' or 'npm run start' from the command line to start the application.
*The server starts at localhost:3000 if no .env(environment) variable has been assigned.
*Open localhost:3000 from the browser and you can find the app up and running.

